﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;

namespace DMShite
{
    public class Program
    {
        static void Main(string[] args)
        {
            string path_s1_pi = "D:\\Documents\\mellennium cohort\\Clean\\S1-PI.csv";
            string path_s2_pi = "D:\\Documents\\mellennium cohort\\Clean\\S2-PI.csv";
            string path_s3_pi = "D:\\Documents\\mellennium cohort\\Clean\\S3-PI.csv";

            string path_s1_dv = "D:\\Documents\\mellennium cohort\\Clean\\S1-DV.csv";
            string path_s2_dv = "D:\\Documents\\mellennium cohort\\Clean\\S2-DV.csv";
            string path_s3_dv = "D:\\Documents\\mellennium cohort\\Clean\\S3-DV.csv";

            //var s1_pi = File.ReadAllLines(path_s1_pi);
            //var s2_pi = File.ReadAllLines(path_s2_pi);

            var s1_pi_headers = HeadersFor(path_s1_pi);
            var s2_pi_headers = HeadersFor(path_s2_pi);
            var s3_pi_headers = HeadersFor(path_s3_pi);

            var s1_dv_headers = HeadersFor(path_s1_dv);
            var s2_dv_headers = HeadersFor(path_s2_dv);
            var s3_dv_headers = HeadersFor(path_s3_dv);

            var pairs_pi_12 = MatchingPairs(s1_pi_headers, s2_pi_headers);
            var pairs_dv_12 = MatchingPairs(s1_dv_headers, s2_dv_headers);

            var pairs_pi_23 = MatchingPairs(s2_pi_headers, s3_pi_headers);
            var pairs_dv_23 = MatchingPairs(s2_dv_headers, s3_dv_headers);

            var pairs_pi_13 = MatchingPairs(s1_pi_headers, s3_pi_headers);
            var pairs_dv_13 = MatchingPairs(s1_dv_headers, s3_dv_headers);

            string final = "";

            final += "\r\n" + ("Pairs-PI-1 & 2:");
            final += "\r\n" + (string.Join("\r\n", pairs_pi_12));

            final += "\r\n" + "\r\n" + ("Pairs-PI-1 & 3:");
            final += "\r\n" + (string.Join("\r\n", pairs_pi_13));

            final += "\r\n" + "\r\n" + ("Pairs-PI-2 & 3:");
            final += "\r\n" + (string.Join("\r\n", pairs_pi_23));

            final += "\r\n" + "\r\n" + ("Pairs-DV-1 & 2:");
            final += "\r\n" + (string.Join("\r\n", pairs_dv_12));

            final += "\r\n" + "\r\n" + ("Pairs-DV-1 & 3:");
            final += "\r\n" + (string.Join("\r\n", pairs_dv_13));

            final += "\r\n" + "\r\n" + ("Pairs-DV-2 & 3:");
            final += "\r\n" + (string.Join("\r\n", pairs_dv_23));
            
            File.WriteAllText("D:\\final.txt", final);
            //DataTable S1_DV_DT = new DataTable();
            //S1_DV_DT.Load(s1_dv_dr);

            //foreach (DataRow row in S1_DV_DT.Rows)
            //{

            //}
        }

        public static string[] HeadersFor(string path)
        {
            var data = File.ReadAllLines(path);

            var data_headers = data[0].Split(',').Select(s => s.Trim('\"')).ToArray();

            return data_headers;
        }

        public static string[] MatchingPairs(string[] h1, string[] h2)
        {
            List<string> pairs = new List<string>();

            foreach (var c in h1)
            {
                if (c.Contains("ADHD") || c.Contains("mcsid") || c.StartsWith("HYPE"))
                    continue;
                
                var matches = h2.Where(d => d.Substring(1).Equals(c.Substring(1)));

                foreach (var d in matches)
                {
                    pairs.Add(d + "-" + c);
                }
            }

            return pairs.ToArray();
        }
    }
}
